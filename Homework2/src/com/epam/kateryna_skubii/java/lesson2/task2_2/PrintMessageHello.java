package com.epam.kateryna_skubii.java.lesson2.task2_2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class PrintMessageHello {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Scanner in = new Scanner(System.in);

        System.out.println("Task № 2");

        /// Создаём новый обьект класса MessageHello
        MessageHello messageHello = new MessageHello();
        /// Присваиваем значения переменным объекта messageHello, которые будут выведены в тексте на экран
        System.out.println("Input a greeting: ");
        messageHello.greeting = in.nextLine();
        System.out.println("Input a name: ");
        messageHello.name = in.nextLine();
        System.out.println("Input a number One: ");
        messageHello.one = in.nextInt();
        System.out.println("Input a number Two: ");
        messageHello.two = in.nextInt();
        System.out.println("Input a number Three: ");
        messageHello.three = in.nextInt();

        /// Создаём объект типа Map<String, Object>
        Map<String, Object> map = new HashMap<>();
        /// Добавляем в объект map элементы
        map.put("messageHello", messageHello);

        /// Распечатывам строку используя метод printInFormat (класса TemplateGenerator) чтобы заполнить её
        /// требуемыми данными (приветствие, имя, целые числа от 1 до 3)
        System.out.println(TemplateGenerator.format("${messageHello.greeting}, ${messageHello.name}. " +
                "${messageHello.one}, " + "${messageHello.two}, ${messageHello.three}", map));
    }
}
