package com.epam.kateryna_skubii.java.lesson2.task2_2;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class TemplateGenerator {
    private static final String FIELD_START = "\\$\\{";
    private static final String FIELD_END = "}";

    private static final String REGEX = FIELD_START + "([^}]+)" + FIELD_END;
    private static final Pattern pattern = Pattern.compile(REGEX);

    private TemplateGenerator(){}

    public static String format(String format, Map<String, Object> objects) throws NoSuchFieldException,
                                                                                    IllegalAccessException {
        String result = format;
        Matcher m = pattern.matcher(format);
        try {
        while (m.find()) {
            String[] found = m.group(1).split("\\.");
            Object o = objects.get(found[0]);
            Field f = o.getClass().getField(found[1]);
            String newVal = f.get(o).toString();
            result = result.replaceFirst(REGEX, newVal);
        }
        } catch(NullPointerException exception){
            System.out.println("Error, unable to find template");
        }
        return result;
    }
}
