package com.epam.kateryna_skubii.java.lesson2.task2_1;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

public enum Student {

    /// Студент Гришин
    GRISHIN("Ivan",
            "Grishin",
            "Humanitarian knowledge",
            LocalDateTime.of(2020, Month.JULY, 15, 10, 0),
            new Course[]{ Course.HISTORY, Course.GEOGRAPHY } ),
    /// Студент Гришин
    VOLNOV("Evgeny",
            "Volnov",
            "Mathematical bias",
            LocalDateTime.of(2020, Month.AUGUST, 1, 10, 0),
            new Course[]{ Course.MATHS, Course.PHYSICS } ),
    /// Студент Гришин
    VETROVA("Anastasia",
            "Vetrova",
            "Science of celestial bodies",
            LocalDateTime.of(2020, Month.JANUARY, 1, 10, 0),
            new Course[]{ Course.ASTRONOMY, Course.PHYSICS, Course.MATHS } ),
    /// Студент Гришин
    MURAVA("Yadviga",
            "Murava",
            "Social institutions",
            LocalDateTime.of(2019, Month.DECEMBER, 31, 10, 0),
            new Course[]{ Course.SOCIOLOGY, Course.HISTORY, Course.GEOGRAPHY } ),
    /// Студент Гришин
    MARTYNIN("Vsevolod",
            "Martynin",
            "Foreign languages",
            LocalDateTime.of(2020, Month.APRIL, 1, 10, 0),
            new Course[]{ Course.ENGLISH, Course.MATHS, Course.PHYSICS,
                    Course.HISTORY, Course.GEOGRAPHY } );

    /// Имя студента
    private final String name;
    /// Фамилия студента
    private final String surname;
    /// Программа обучения - ее название
    private final String curriculum;
    /// Начало обучения Студента
    private final LocalDateTime startDate;
    /// Хранит в себе курсы, которые прошел или проходит Студент
    private final Course[] courses;

    Student(
            String name,
            String surname,
            String curriculum,
            LocalDateTime startDate,
            Course[] courses
    ) {
        this.name = name;
        this.surname = surname;
        this.curriculum = curriculum;
        this.startDate = startDate;
        this.courses = courses;
    }

    public void printReport() {
        printReport(false);
    }

    /// Возвращает сокращенную информацию о студенте
    /// Parameter isDetailed: - будет ли выводиться подробные отчет
    /// Returns: строку с сокращенной информацией о студенте в виде
    /// "Ivanov Ivan (Java Developer) - Обучение не закончено. До окончания осталось 1 д 6 ч"
    public void printReport(boolean isDetailed) {
        final int HOURS_IN_DAY = 24;
        final int STARTING_WORKING_HOUR = 10; /// Учебный день начинаеться в 10 утра
        final int ENDING_WORKING_HOUR = 18; /// Учебный день заканчиваеться в 18
        final int WORKING_HOURS_PER_DAY = ENDING_WORKING_HOUR - STARTING_WORKING_HOUR; /// В одном дне 8 учебных часов

        // / 1. Находим endDate окончания курса
        /// 1.1 Находим количество целых дней курса
        int courseFullDaysAmount = Math.floorDiv(getCoursesDuration(), WORKING_HOURS_PER_DAY);
        int courseHoursRemains = getCoursesDuration() % WORKING_HOURS_PER_DAY;
        /// 1.2 Добавляем к Дате начала обучения длительность курса в днях
        LocalDateTime courseEndDateTime = startDate.plusDays(courseFullDaysAmount).plusHours(courseHoursRemains);

        /// 2 Заполняем строку о окончании курса
        /// 2.1 Вычисляем статус завершенности курса
        if (LocalDateTime.now().isAfter(courseEndDateTime)) {
            /// Если время до окончания курса положительное - Обучение еще не закончено
            /// 2.1.1 Находим количество часов после окончания курса
            long hoursAfterCourseEnd = Duration.between(courseEndDateTime, LocalDateTime.now()).toHours();
            long daysToCourseEndAmount = Math.floorDiv(hoursAfterCourseEnd, HOURS_IN_DAY);
            long remainingHoursAfterFullDays = hoursAfterCourseEnd - (daysToCourseEndAmount * HOURS_IN_DAY);

            if (isDetailed) {
                String timeExplanation = "Time left: " + daysToCourseEndAmount + "d "
                        + remainingHoursAfterFullDays + "h";
                printDetailedCourseReport(getCoursesDuration(), courseEndDateTime, timeExplanation);
            } else {
                String educationStatusText = "Education completed. Time after course finishing: " + daysToCourseEndAmount +
                        "d " + remainingHoursAfterFullDays + "h";
                printShortCourseReport(educationStatusText);
            }
        } else {
            /// Если время до окончания курса отрицательное - Обучения еще не завершено
            /// 2.1.2 Узнаем количество учебных часов до окончания курса
            int educationHoursToCourseEnd = 0;

            /// 2.1.2.1 Если первый учебный день еще не закончился - Узнаем количество учебных часов до конца
            // текущего дня
            if (LocalDateTime.now().getHour() < ENDING_WORKING_HOUR) {
                educationHoursToCourseEnd += ENDING_WORKING_HOUR - LocalDateTime.now().getHour();
            }

            /// 2.1.2.2 Узнаем количество учебных часов со второго и до предпоследнего дня обучения включительно
            LocalDateTime tomorrowDate = LocalDate.now().plusDays(1).atStartOfDay();
            LocalDateTime preLastDate = courseEndDateTime.toLocalDate().atStartOfDay();
            educationHoursToCourseEnd += Duration.between(tomorrowDate, preLastDate).toDays() * WORKING_HOURS_PER_DAY;

            /// 2.1.2.3 Если последний учебный день еще не закончился - Узнаем количество учебных часов
            // в последний учебный день
            educationHoursToCourseEnd += courseEndDateTime.getHour() - STARTING_WORKING_HOUR;

            int daysToCourseEndAmount = Math.floorDiv(educationHoursToCourseEnd, WORKING_HOURS_PER_DAY);
            int remainingHoursAfterFullDays = educationHoursToCourseEnd - (daysToCourseEndAmount * WORKING_HOURS_PER_DAY);

            if (isDetailed) {
                int timeSpentOnCourse = getCoursesDuration() - educationHoursToCourseEnd;
                String timeExplanation = "Time after course finishing: " + daysToCourseEndAmount + "d "
                        + remainingHoursAfterFullDays + "h";
                printDetailedCourseReport(timeSpentOnCourse, courseEndDateTime, timeExplanation);
            } else {
                String educationStatusText = "Education is not complete. Time left: "
                        + daysToCourseEndAmount + "d " + remainingHoursAfterFullDays + "h";
                printShortCourseReport(educationStatusText);
            }
        }
    }

    private void printDetailedCourseReport(
            int timeSpentOnCourse,
            LocalDateTime courseEndDateTime,
            String timeExplanation) {
        System.out.println(" FIO: " + name + " " + surname +
                "\n Time spent: " + timeSpentOnCourse + "h" +
                "\n CURRICULUM: " + curriculum +
                "\n Course duration: " + getCoursesDuration() + "h" +
                "\n START_DATE: " + startDate.toLocalDate() +
                "\n END_DATE: " + courseEndDateTime.toLocalDate() +
                "\n " + timeExplanation + "\n");
    }

    private void printShortCourseReport(String educationStatusText) {
        System.out.println(name + " " + surname + " (" + curriculum + ") - " + educationStatusText + "\n");
    }

    private int getCoursesDuration() {
        int coursesDuration = 0;
        for (Course course : courses) {
            coursesDuration += course.getDuration();
        }
        return coursesDuration;
    }

}
