package com.epam.kateryna_skubii.java.lesson2.task2_1;

public enum Course {

    /// Курс Истории
    HISTORY("History", 70),
    /// Курс Географии
    GEOGRAPHY("Geography", 120),
    /// Курс Математики
    MATHS("Maths", 60),
    /// Курс Физики
    PHYSICS("Physics", 170),
    /// Курс Английского языка
    ENGLISH("English", 350),
    /// Курс Астрономии
    ASTRONOMY("Astronomy", 180),
    /// Курс Социологии
    SOCIOLOGY("Sociology", 55);

    /// Делает
    /// Returns: - строку с описанием завершен ли курс и сколько осталось времени или прошло времени с
    // моментна завершения курса
    public Integer getDuration() {
        return duration;
    }

    /// Имя курса
    private final String name;
    /// Продолжительность курса в часах
    private final Integer duration;

    Course(String name, Integer duration) {
        this.name = name;
        this.duration = duration;
    }
}
