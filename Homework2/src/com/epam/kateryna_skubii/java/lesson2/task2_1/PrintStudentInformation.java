package com.epam.kateryna_skubii.java.lesson2.task2_1;

public class PrintStudentInformation {

    public static void main(String[] args) {

        System.out.println("Задача № 1");

        Student.GRISHIN.printReport(false);
        Student.VOLNOV.printReport(true);
        Student.VETROVA.printReport();
        Student.MURAVA.printReport(true);
        Student.MARTYNIN.printReport();
    }
}
